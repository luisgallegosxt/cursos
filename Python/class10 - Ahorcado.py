import random
import getch

IMAGES = ['''
      _______
     |/      |
     |      
     |      
     |      
     |      
     |
    _|___

'''
,
'''
      _______
     |/      |
     |      (_)
     |      
     |      
     |      
     |
    _|___

'''
,
'''
      _______
     |/      |
     |      (_)
     |       |
     |       
     |       
     |
    _|___

'''
,
'''
      _______
     |/      |
     |      (_)
     |      \|
     |       
     |       
     |
    _|___

'''

,'''
      _______
     |/      |
     |      (_)
     |      \|/
     |       
     |       
     |
    _|___

'''
,
'''
      _______
     |/      |
     |      (_)
     |      \|/
     |       |
     |       
     |
    _|___

'''
,
'''
      _______
     |/      |
     |      (_)
     |      \|/
     |       |
     |      / 
     |
    _|___

'''
,
'''
      _______
     |/      |
     |      (_)
     |      \|/
     |       |
     |      / \     
    _|___

'''
]


def print_tittle():
    print(''' 
     _                                             
    | |                                            
    | |__   __ _ _ __   __ _ _ __ ___   __ _ _ __  
    | '_ \ / _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
    | | | | (_| | | | | (_| | | | | | | (_| | | | |
    |_| |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                        __/ |                      
                    |___/                       
    ''')


WORDS = [
    'lavadora',
    'secadora',
    'sofa',
    'gobierno',
    'diputado',
    'democracia',
    'computadora',
    'teclado'
]    

def random_word():
    index = random.randint(0,len(WORDS)-1)
    return WORDS[index]


def display_board(hidden_word,tries):
    print(IMAGES[tries])
    print('')
    print(hidden_word)
    print(' --- * --- * --- * --- * --- * --- ')

def main():
    print_tittle()

    word = random_word()
    hidden_word = ['-'] * len(word)
    tries = 0

    selected_word = []
    current_letter = ''

    while True:

        display_board(hidden_word, tries)        
        
        print('Letras que ya fueron seleccionadas: {}'.format(selected_word))
        #Solo permite ingresar una letra a la vez
        print('Selecciona una letra:')
        current_letter_pre = getch.getch()
        equal = False
        ##valida que la letra seleccionada, no haya sido seleccionada antes
        for i in range(len(selected_word)):
            if current_letter_pre == selected_word[i]:
                equal = True
            else:
                equal = False
        if equal == False:
            current_letter = current_letter_pre
            selected_word.append(current_letter)

            letter_indexes = []
            for i in range(len(word)):
                if word[i] == current_letter:
                    letter_indexes.append(i)

            if len(letter_indexes) == 0:
                tries += 1
            else:
                for idx in letter_indexes:
                    hidden_word[idx] = current_letter

                letter_indexes = []

            success_word = ''.join(hidden_word)
            ##Mensaje final sobre el resultado del jugador
            if success_word == word:
                print('Felicidades, has encontrado la palabra correcta')
                break
            elif tries == len(IMAGES):
                print('Lo sentimos, has perdido el juego. La palabra correcta es: "{}" Puedes volver a intentarlo'.format(word))
                break
        
        else:
            print('La letra {} ya fue seleccionada, intenta nuevamente'.format(current_letter_pre))                  

        
if __name__ == '__main__':
    main()