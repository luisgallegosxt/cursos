# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 21:52:55 2020

@author: Usuario
"""

def funcion_decoradora(funcion_parametro):
    
    def funcion_interior():
        print("Mensaje antes de las funciones")
        
        funcion_parametro()
        
        print("Mensaje después de las funciones\n\n")
        
    return funcion_interior


@funcion_decoradora
defsuma():
    print(10+20)


@funcion_decoradora
defresta():
    print(10-20)


@funcion_decoradora
defmultiplicacion():
    print(10*20)


@funcion_decoradora
defdivision():
    print(10/20)


@funcion_decoradora
defpotencia():
    print(10**20)



if __name__ == "__main__":
    suma()
    resta()
    multiplicacion()
    division()
    potencia()