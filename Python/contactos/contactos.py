# -*- coding: utf-8 -*-
import csv

class Contact:

    def __init__(self, name, phone, email):
        self.name = name
        self.phone = phone
        self.email = email
class ContactBook:

    def __init__(self):
        self._contacts = []

    def _save(self):
        with open('contacts.csv', 'w', newline='',encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerow( ('name','phone', 'email') )

            for contact in self._contacts:
                writer.writerow( (contact.name, contact.phone, contact.email ) )

    def add(self, name, phone, email):
        contact = Contact(name, phone, email)
        self._contacts.append(contact)
        self._save()

    def show_all(self):
        for contact in self._contacts:
            self._print_contact(contact)

    def _print_contact(self, contact):
        print('---*---*---*---*---*---*---*---')
        print('Nombre: {}'.format(contact.name))
        print('Telefono: {}'.format(contact.phone))
        print('Email: {}'.format(contact.email))
        print('---*---*---*---*---*---*---*---')

    def delete(self, name):
        for idx, contact in enumerate(self._contacts):
            if contact.name.lower() == name.lower():
                del self._contacts[idx]
                self._save()
                break

    def search(self, name):
        for contact in self._contacts:
            if contact.name.lower() == name.lower():
                self._print_contact(contact)
                break
        else:
            self._not_found()

    def search_to_update(self, name):
        for idxs, contact in enumerate(self._contacts):
            if contact.name.lower() == name.lower():
                return idxs 
                break
        else:
            return False

    def _not_found(self):
        print('No se ha encontrado el contacto')

    def update(self, idxs, name, phone, email):
        for idx, contact in enumerate(self._contacts):
            if idx == idxs:
                contact.name = name
                contact.phone = phone
                contact.email = email
                self._save()
                break

def main():

    contact_book = ContactBook()
    try:
        with open('contacts.csv', 'r', newline='' ,encoding="utf-8") as f:
            reader = csv.reader(f)
            for idx, row in enumerate(reader):
                if idx==0:
                    continue
                
                contact_book.add(row[0], row[1], row[2])
    except IOError:
        print('')
        print('******NOTIFICACION******')
        print('Se ha creado el archivo para almacenar los contactos')

    while True:
        command = str(input('''
            ¿Qué deseas hacer?

            [a]ñadir contacto
            [ac]tualizar contacto
            [b]uscar contacto
            [e]liminar contacto
            [l]istar contactos
            [s]alir
        '''))

        if command == 'a':
            name = str(input('Escribe el nombre del contacto: '))
            phone = str(input('Escrine el telefono del contacto: '))
            email = str(input('Escrine el email del contacto: '))

            contact_book.add(name, phone, email)

        elif command == 'ac':
            name = str(input('Escriba el nombre del contacto que desee actualizar: '))
            if contact_book.search_to_update(name) is not None:
                idx = contact_book.search_to_update(name)
                name = str(input('Escribe el nuevo nombre del contacto: '))
                phone = str(input('Escrine el nuevo telefono del contacto: '))
                email = str(input('Escrine el nuevo email del contacto: '))
                contact_book.update(idx, name, phone, email)
            else:       
                contact_book._not_found()

        elif command == 'b':
            name = str(input('Escriba el nombre del contacto a buscar: '))
            contact_book.search(name)

        elif command == 'e':
            name = str(input('Escriba el nombre del contacto a eliminar: '))
            contact_book.delete(name)

        elif command == 'l':
            contact_book.show_all()

        elif command == 's':
            break
        else:
            print('Comando no encontrado.')


if __name__ == '__main__':
    main()
