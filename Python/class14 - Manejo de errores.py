# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 13:22:58 2020

@author: Luis Gallegos
"""

countries = {
    'mexico': 122,
    'colombia': 49,
    'argentina': 43,
    'chile': 18,
    'peru': 31
}


while True:
    country = str(input('Escribe el nombre de un pais: ')).lower()
    
    try:
        print('La poblacion de {} es: {} millones'.format(country, countries[country]))
    except KeyError:
        print('No tenemos el dato de la problacion de {}'.format(country))
    