# -*- coding: utf-8 -*-

def foreign_exchange_calculator(ammont):
    mex_to_col_rate = 145.97

    return mex_to_col_rate * ammont

def main():
    print('CALCULADORA DE DIVISAS')
    print('Convierte Pesos Mexicanos a Pesos Colombianos.')
    print('')

    ammount = float(input('Ingresa la cantidad de pesos mexicanos que quieres convertir: '))

    result = foreign_exchange_calculator(ammount)

    print('${} pesos mexicanos son ${} pesos colombianos'.format(ammount,result))
    print('')



if __name__ == '__main__':
    main()