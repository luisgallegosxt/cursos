# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 16:03:08 2020

@author: Luis Gallegos
"""

def cant_repeticiones():
    counter = 0
    with open('aleph.txt', 'r', encoding='utf-8') as f:
        for line in f:
            counter += line.count('Beatriz')
    
    print('Beatriz se encuentra {} en el texto'.format(counter))



def main():
    cant_repeticiones()




if __name__ == '__main__':
    main()