
def palindrome(word):
    reverse_letters = []

    for letter in word:
        reverse_letters.insert(0, letter)

    reverse_word = ''.join(reverse_letters)
    
    if reverse_word == word:
        return True
    else:
        return False

def main():
    word = str(input('Escribe una palabra: '))

    result = palindrome(word)

    if result is True:  
        print('Tu palabra {} si es un palindromo'.format(word))
    else:
        print('Tu palabra {} no es un palindromo'.format(word))






if __name__ == '__main__':
    main()