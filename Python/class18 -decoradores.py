# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 21:47:00 2020

@author: Luis Gallegos
""" 
def protected(func):
    
    def wrapper(password):
        if password == 'platzi':
            return func()
        else:
            print('La contraseña es incorrecta')
            
    return wrapper

@protected
def protected_func():
    print('Tu contraseña es correcta')
    
def main():
    password = str(input('Ingresa tu contraseña: '))
    
    protected_func(password)
    
    
if __name__ == '__main__':
    main()