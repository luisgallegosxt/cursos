
def factorial(number):
    if number==0:
        return 1
    
    return number * factorial(number-1)

def main():
    number = int(input('Escribe un numero: '))

    result = factorial(number)

    print(result)



if __name__ == '__main__':
    main()