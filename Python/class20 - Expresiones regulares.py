# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 14:37:38 2020

@author: Luis Gallegos
"""

# import re

# cadena = 'Vamos a aprender expresiones regulares en Python. Gracias Python'

# #print(re.search('aprender',cadena))

# textoBuscar = 'Python'

# # if re.search(textoBuscar,cadena) is not None:
# #     print('He encontrado el texto')
# # else:
# #     print('No he encontrado el texto') 
    
# textoEncontrado = re.search(textoBuscar, cadena)
# textoEncontrado_p = re.findall(textoBuscar, cadena)

# print(textoEncontrado.start())
# print(textoEncontrado.end())
# print(textoEncontrado.span())
# print(textoEncontrado_p)

#march busca siempe al comienzo
#re.match('.ara', nombre3, re.IGNORECASE) va a encontrar Jara y Lara, ya que el punto representa cualquier cosa
#re.match('\d', nombre3)                  Para encontrar todo lo que empiece con un digito (Numero)


####################################

import re

lista_nombres = ['Ana Gómez',
                 'María Martín',
                 'Sandra López',
                 'Santiago Martín']

for elemento in lista_nombres:
    if re.findall('Santiago', elemento):
        print(elemento)


#re.findall('^Santiago', elemento)  comienza con
#re.findall('Martín$', elemento)    termina con
#re.findall('[ñ]', elemento)        Busca el o los caracteres con la letra buscada
#re.findall('niñ[oa]s', elemento)   Busca el patron, pero entre corchetes puede ser o ó a
#re.findall('[o-t]', elemento)      Rangos 
#re.findall('^[O-T]', elemento)     Rangos que comiencen con esas letras
#re.findall('Ma[^0-3]', elemento)   Para negar, nos podria arrojar del 4 en adelante
#re.findall('Ma[0-3A-B]', elemento)   Para negar, nos podria arrojar del 4 en adelante, se puede agregar mas rangos sin la necesidad de poner ','
    