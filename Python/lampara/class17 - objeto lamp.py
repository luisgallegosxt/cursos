# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 20:29:55 2020

@author: Luis Gallegos
"""

from lamp import Lamp

def main():
    lamp = Lamp(is_turned_on = False)  

    while True:
        command = str(input('''
            ¿Qué deseas hacer?

            [p]render
            [a]pagar
            [s]alir
        '''))

        if command == 'p':
            lamp.turn_on()
        elif command == 'a':
            lamp.turn_off()
        else:
            break
          
            
            
if __name__ == '__main__':
    main()