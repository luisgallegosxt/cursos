# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 14:52:30 2020

@author: Luis Gallegos
"""


def main():
    with open('numeros.txt', 'w') as f:
        for i in range(10):
            f.write(str(i))


if __name__ == '__main__':
    main()